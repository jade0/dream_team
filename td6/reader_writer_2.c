#include "reader_writer.h"
#include "reader_writer_tracing.h"
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>

extern tracing_t t;

struct reader_writer
{
        pthread_mutex_t readers_nb_mutex;
        pthread_mutex_t writter_mutex;
        int readers_nb;
        bool writter;
        pthread_cond_t wait_read_cond;
        pthread_cond_t wait_write_cond;
};

typedef struct reader_writer reader_writer;

reader_writer_t rw_init()
{
    reader_writer_t rw = malloc(sizeof(reader_writer));
    pthread_mutex_init(&rw->readers_nb_mutex, NULL);
    pthread_mutex_init(&rw->writter_mutex, NULL);
    rw->readers_nb = 0;
    rw->writter = false;
    pthread_cond_init(&rw->wait_read_cond,NULL);
    pthread_cond_init(&rw->wait_write_cond,NULL);
    return rw;
}

void begin_read(reader_writer_t rw)
{
    pthread_mutex_lock(&rw->writter_mutex);
    while (rw->writter == true)
    {
        tracing_record_event(t, WR_EVENT_ID);
        pthread_cond_wait(&rw->wait_read_cond, &rw->writter_mutex);
    }

    pthread_mutex_lock(&rw->readers_nb_mutex);
    rw->readers_nb++;
    tracing_record_event(t, BR_EVENT_ID);
    pthread_mutex_unlock(&rw->readers_nb_mutex);
    pthread_mutex_unlock(&rw->writter_mutex);
    pthread_cond_broadcast(&rw->wait_read_cond);
}

void end_read(reader_writer_t rw)
{

    pthread_mutex_lock(&rw->readers_nb_mutex);
    rw->readers_nb--;
    tracing_record_event(t, ER_EVENT_ID);
    pthread_mutex_unlock(&rw->readers_nb_mutex);
    pthread_cond_signal(&rw->wait_write_cond);
}

void begin_write(reader_writer_t rw)
{
    pthread_mutex_lock(&rw->writter_mutex);
    while (rw->writter != false)
    {
        tracing_record_event(t, WWW_EVENT_ID);
        pthread_cond_wait(&rw->wait_write_cond, &rw->writter_mutex);
    }
    rw->writter = true;
    pthread_mutex_unlock(&rw->writter_mutex);
    pthread_mutex_lock(&rw->readers_nb_mutex);
    while (rw->readers_nb != 0)
    {
        tracing_record_event(t, WWR_EVENT_ID);
        pthread_cond_wait(&rw->wait_write_cond, &rw->readers_nb_mutex);
    }
    tracing_record_event(t, BW_EVENT_ID);
    pthread_mutex_unlock(&rw->readers_nb_mutex);
}

void end_write(reader_writer_t rw)
{

    pthread_mutex_lock(&rw->writter_mutex);
    rw->writter = false;
    tracing_record_event(t, EW_EVENT_ID);
    pthread_mutex_unlock(&rw->writter_mutex);
    pthread_cond_signal(&rw->wait_write_cond);
    pthread_cond_broadcast(&rw->wait_read_cond);
}

