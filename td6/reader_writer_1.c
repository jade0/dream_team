#include "reader_writer.h"
#include "reader_writer_tracing.h"
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>

extern tracing_t t;

struct reader_writer
{
    pthread_mutex_t list_mutex;
};

typedef struct reader_writer reader_writer;

reader_writer_t rw_init()
{
    reader_writer_t rw = malloc(sizeof(reader_writer));
    pthread_mutex_init(&rw->list_mutex, NULL);
    return rw;
}

void begin_read(reader_writer_t rw)
{
    pthread_mutex_lock(&rw->list_mutex);
    tracing_record_event(t, BR_EVENT_ID);
}

void end_read(reader_writer_t rw)
{
    tracing_record_event(t, ER_EVENT_ID);
    pthread_mutex_unlock(&rw->list_mutex);
}

void begin_write(reader_writer_t rw)
{

    pthread_mutex_lock(&rw->list_mutex);
    tracing_record_event(t, BW_EVENT_ID);
}

void end_write(reader_writer_t rw)
{
    tracing_record_event(t, EW_EVENT_ID);
    pthread_mutex_unlock(&rw->list_mutex);
}

