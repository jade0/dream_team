TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
LIBS = -lpthread

SOURCES += \
    linked_list.c \
    linked_list_simple_test.c \
    reader_writer_tracing.c \
    reader_writer_2.c

HEADERS += \
    linked_list.h \
    reader_writer.h \
    reader_writer_tracing.h

