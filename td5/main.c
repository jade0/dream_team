#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

int eating_duration = 2;
int max_thinking_duration = 5;
int N;
bool* chops;
pthread_mutex_t chops_mutex;
pthread_cond_t chops_cond;

void think(int index, int thinking_duration);

void take_chops(int i)
{
    pthread_mutex_lock(&chops_mutex);
    while (chops[i] == false || chops[(i + 1) % N] == false)
    {
        pthread_cond_wait(&chops_cond, &chops_mutex);
    }
    chops[i] = false;
    chops[(i + 1) % N] = false;
    pthread_mutex_unlock(&chops_mutex);
    pthread_cond_broadcast(&chops_cond);
}

void put_down_chops(int i)
{
    pthread_mutex_lock(&chops_mutex);
    chops[i] = true;
    chops[(i + 1) % N] = true;
    pthread_mutex_unlock(&chops_mutex);
    pthread_cond_broadcast(&chops_cond);
}

void eat(int i, int remaining_eating_duration)
{
    take_chops(i);
    printf("Philosopher %d started eating\n", i);
    int ret_time = time(NULL) + remaining_eating_duration;
    while (time(NULL) < ret_time)
    {
        if ((rand() % 50000000) == 5)
        {
            printf("Philosopher %d starts thinking (during eating)\n", i);
            put_down_chops(i);
            int eating_duration_left = ret_time - time(NULL);
            think(i, rand() % max_thinking_duration);
            printf("Philosopher %d finishes thinking (needs to finish eating)\n", i);
            eat(i, eating_duration_left);
            return;
        }
    }
    printf("Philosopher %d finished eating\n", i);
    put_down_chops(i);
}

void think(int index, int thinking_duration)
{
    printf("Philosopher %d starts thinking\n", index);
    int ret_time = time(NULL) + thinking_duration;
    while (time(NULL) < ret_time);
    printf("Philosopher %d finishes thinking\n", index);
}

void* philosopher_thread(void* data)
{
    int* index = (int*)data;
    think(*index, rand() % max_thinking_duration);
    eat(*index, eating_duration);
    return NULL;
}

int main(int argc, char** argv)
{
    if (argc < 2) printf ("Usage ./philosopher N\n");
    else
    {
        N = atoi(argv[1]);
        srand(time(NULL));

        pthread_t* philosophers = malloc(sizeof(pthread_t) * N);
        int* philosophers_indexes = malloc(sizeof(int) * N);
        chops = malloc(sizeof(bool) * N);

        pthread_mutex_init(&chops_mutex, NULL);
        pthread_cond_init(&chops_cond, NULL);
        int i = 0;
        for (i = 0 ; i < N ; i++)
        {
            chops[i] = true;
            philosophers_indexes[i] = i;
        }
        for (i = 0 ; i < N ; i++)
        {
            pthread_create(&philosophers[i], NULL, philosopher_thread, &philosophers_indexes[i]);
        }
        for (i = 0 ; i < N ; i++)
        {
            pthread_join(philosophers[i], NULL);
        }
        pthread_mutex_destroy(&chops_mutex);
        pthread_cond_destroy(&chops_cond);

        free(philosophers);
        free(philosophers_indexes);
        free(chops);
    }
    return 0;
}

