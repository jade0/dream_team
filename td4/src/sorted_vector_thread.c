#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <pthread.h>

bool found = false;

struct thread_arg_t
{
	long int* num_array;
	long int first_index;
	long int last_index;
	long int searched_value;
};
typedef struct thread_arg_t thread_arg_t;

void* search(void* void_arg)
{
	thread_arg_t* arg = (thread_arg_t*) void_arg;
	long int i;
	for (i = arg->first_index ; !found && i < arg->last_index ; i++)
	{
		if (arg->num_array[i] == arg->searched_value)
		{
			printf("%ld is in the vector at index %ld\n", arg->searched_value, i);
			found = true;
			return NULL;
		}
	}
	return NULL;
}
int  main (int argc, char** argv)
{
	if( argc<3)
	{
		printf("Usage : ./sorted_vector_thread <number_of_thread> <size of the vector> <number searched>\n");
		return 1;
	}
	srand(time(NULL));
	long int thread_number = atol(argv[1]);
	long int n = atol(argv[2]);
	long int number_per_thread = n / thread_number;
	long int x = atol(argv[3]);
	long int* T = malloc(n*sizeof(long int));
	long int i;
	
	pthread_t* tids = malloc (thread_number*sizeof(pthread_t)) ;
	thread_arg_t* args = malloc (thread_number*sizeof(thread_arg_t));
	
	for (i=0;i<n;i++)
	{
		T[i] = 	rand()%n;
	}
	
	long int current_index = 0;
	for (i = 0 ; i < thread_number-1 ; i++)
	{
		args[i].first_index = current_index;
		args[i].last_index = current_index + number_per_thread;
		args[i].num_array = T;
		args[i].searched_value = x;
		pthread_create(&tids[i], NULL, search, &args[i]);
		current_index += number_per_thread;
	}
	args[i].first_index = current_index;
	args[i].last_index = n ;
	args[i].num_array = T;
	args[i].searched_value = x;
	pthread_create(&tids[i], NULL, search, &args[i]);
	
	for (i = 0 ; i < thread_number ; i++)
	{
		pthread_join(tids[i], NULL);
	}
	
	if (!found) printf("%ld is not in the vector\n", x);
	
	return 0;
}
