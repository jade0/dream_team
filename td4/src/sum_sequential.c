#include <stdlib.h>
#include <stdio.h>

int main (int argc, char** argv)
{
	int n = argc;
	int num_array[n];
	int i = 0;
	for (i = 1 ; i < n ; i++)
	{
		num_array[i] = atoi(argv[i]);
	}
	
	int sum = 0;
	for (i = 1 ; i < n ; i++)
	{
		sum += num_array[i];
	}
	
	printf("Sum : %i\n", sum);
	
	return EXIT_SUCCESS;
}
