#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

/* thread parameters structure */
struct thread_arg_t
{
	long int* num_array;
	long int first_index;
	long int last_index;
	long long int sum;
};
typedef struct thread_arg_t thread_arg_t;

void* compute_sum(void* args)
{
	thread_arg_t* arg = (thread_arg_t*) args;
	long int i = 0;
	for (i = arg->first_index ; i < arg->last_index ; i++)
	{
		arg->sum += arg->num_array[i];
	} 
	return NULL;
}

int main (int argc, char** argv)
{
	if (argc != 2)
	{
		printf("Usage : ./sum_thread <thread_number>\n");
		return -1;
	}
	long long int sum = 0;
	long int n = 200000000;
	long int* num_array = malloc(n * sizeof(long int));
	long int thread_number = atoi(argv[1]);
	long int number_per_thread = n / thread_number;
	pthread_t* tids = malloc(thread_number*sizeof(pthread_t)) ;
	thread_arg_t* args = malloc(thread_number*sizeof(thread_arg_t));
	long int i = 0;
	
	for (i = 0 ; i < n ; i++)
	{
		num_array[i] = i;
	}
	
	long int current_index = 0;
	for (i = 0 ; i < thread_number - 1 ; i++)
	{
		args[i].first_index = current_index;
		args[i].last_index = current_index + number_per_thread;
		args[i].num_array = num_array;
		args[i].sum = 0;
		pthread_create(&tids[i], NULL, compute_sum, &args[i]);
		current_index += number_per_thread;
	}
	args[i].first_index = current_index;
	args[i].last_index = n;
	args[i].num_array = num_array;
	args[i].sum = 0;
	pthread_create(&tids[i], NULL, compute_sum, &args[i]);
	
	for (i = 0 ; i < thread_number ; i++)
	{
		pthread_join(tids[i], NULL);
		sum += args[i].sum;
	}

  	free(tids);
  	free(args);
  	free(num_array);
  	
	printf("Sum : %lld\n", sum);
	
	return EXIT_SUCCESS;

}

