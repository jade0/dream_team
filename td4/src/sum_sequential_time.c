#include <stdlib.h>
#include <stdio.h>

int main ()
{
	long int n = 200000000;
	long int* num_array = malloc(n * sizeof(long int));
	long int i = 0;
	for (i = 0 ; i < n ; i++)
	{
		num_array[i] = i;
	}
	
	long long int sum = 0;
	for (i = 0 ; i < n ; i++)
	{
		sum += num_array[i];
	}
	
	printf("Sum : %lld\n", sum);
	
	free(num_array);
	
	return EXIT_SUCCESS;
}
