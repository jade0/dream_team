#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

struct thread_arg_t
{
	int* num_array;
	int first_index;
	int last_index;
};
typedef struct thread_arg_t thread_arg;

struct thread_ret_t
{
	int sum;
	int first_index;
	int last_index;
};
typedef struct thread_ret_t thread_ret;

thread_ret* thread_sums;
int thread_sums_index = 0;

void* compute_sum(void* args)
{
	thread_arg* arg = (thread_arg*) args;
	int sum = 0;
	int i = 0;
	for (i = arg->first_index ; i < arg->last_index ; i++)
	{
		sum += arg->num_array[i];
	}
	int current_index = thread_sums_index++;
	thread_sums[current_index].sum = sum;
	thread_sums[current_index].first_index = arg->first_index;
	thread_sums[current_index].last_index = arg->last_index;
	return NULL;
}

int main (int argc, char** argv)
{
	if (argc < 3)
	{
		printf("Usage : ./sum_thread <thread_number> <nums...>\n");
		return -1;
	}
	int sum=0;
	int n = argc-2;
	int num_array[n];
	int i=1;
	for (i=2; i<argc;i++)
	{
		num_array[i-2]=atoi(argv[i]);
	}
	
	int thread_number = atoi(argv[1]);
	
	thread_sums = malloc(thread_number*sizeof(thread_ret));
	
	pthread_t* tids = malloc (thread_number*sizeof(pthread_t)) ;
	thread_arg* args = malloc (thread_number*sizeof(thread_arg));
	
	int number_per_thread = n / thread_number;
	int current_index = 0;
	
	for (i = 0 ; i < thread_number-1 ; i++)
	{
		args[i].first_index = current_index;
		args[i].last_index = current_index + number_per_thread;
		args[i].num_array = num_array;
		pthread_create(&tids[i], NULL, compute_sum, &args[i]);
		current_index += number_per_thread;
	}
	args[thread_number-1].first_index = current_index;
	args[thread_number-1].last_index = n ;
	args[thread_number-1].num_array = num_array;
	pthread_create(&tids[i], NULL, compute_sum, &args[thread_number-1]);
	
	for (i = 0 ; i < thread_number ; i++)
	{
		pthread_join(tids[i], NULL);
	}
	
	for (i = 0 ; i < thread_sums_index ; i++)
	{
		printf("Thread sum [%i, %i] : %i\n", thread_sums[i].first_index, thread_sums[i].last_index, thread_sums[i].sum);
		sum += thread_sums[i].sum;
	}

  	free (tids) ;
	printf("Sum : %i\n", sum);
	
	return EXIT_SUCCESS;

}

