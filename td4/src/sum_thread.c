#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

/* thread parameters structure */
struct thread_arg_t
{
	int* num_array;
	int first_index;
	int last_index;
};
typedef struct thread_arg_t thread_arg_t;

void* compute_sum(void* args)
{
	thread_arg_t* arg = (thread_arg_t*) args;
	int sum = 0;
	int i = 0;
	for (i = arg->first_index ; i < arg->last_index ; i++)
	{
		sum += arg->num_array[i];
	} 
	return (void*)sum;
}

int main (int argc, char** argv)
{
	if (argc < 3)
	{
		printf("Usage : ./sum_thread <thread_number> <nums...>\n");
		return -1;
	}
	int sum = 0;
	int n = argc - 2;
	int num_array[n];
	int thread_number = atoi(argv[1]);
	int number_per_thread = n / thread_number;
	pthread_t* tids = malloc(thread_number * sizeof(pthread_t)) ;
	thread_arg_t* args = malloc(thread_number * sizeof(thread_arg_t));
	int i = 0;
	
	for (i = 2 ; i < argc ; i++)
	{
		num_array[i-2] = atoi(argv[i]);
	}
	
	int current_index = 0;
	for (i = 0 ; i < thread_number - 1 ; i++)
	{
		args[i].first_index = current_index;
		args[i].last_index = current_index + number_per_thread;
		args[i].num_array = num_array;
		pthread_create(&tids[i], NULL, compute_sum, &args[i]);
		current_index += number_per_thread;
	}
	args[i].first_index = current_index;
	args[i].last_index = n ;
	args[i].num_array = num_array;
	pthread_create(&tids[i], NULL, compute_sum, &args[i]);
	
	for (i = 0 ; i < thread_number ; i++)
	{
		int tmp_sum = 0;
		pthread_join(tids[i], (void**)&tmp_sum);
		sum += tmp_sum;
	}

  	free(tids);
  	free(args);
	printf("Sum : %i\n", sum);
	
	return EXIT_SUCCESS;
}

