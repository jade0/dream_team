#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int search(long int* T,long int n,long int x)
{
	long int i;
	for ( i=0;i<n;i++)
	{
	if (T[i] ==x)
		{
			printf("%ld is in the vector at index %ld\n",x, i);
			return 0;
		}
	}
	printf("%ld isn't in the vector \n",x);
	return -1;
}
int  main (int argc, char** argv)
{
	if( argc<3)
	{
		printf("Usage : ./sorted_vector <size of the vector> <number searched>\n");
		return 1;
	}
	srand(time(NULL));
	long int n = atol(argv[1]);
	long int* T = malloc(n*sizeof(long int));
	long int i;
	for (i=0;i<n;i++)
	{
		T[i] = 	rand()%n;
	}
	return search(T, n, atol(argv[2]));
}
