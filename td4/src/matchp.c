#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

/* threads parameters structure */
struct team_song_t
{
	char* lyrics;
	int repeat_time;
};
typedef struct team_song_t team_song_t;

void* supporter (void* arg)
{
  team_song_t* song = (team_song_t*) arg ;
  int pid = getpid();
  pthread_t tid = pthread_self();
  srand((int)tid);
  int i = 0;
  for (i = 0; i < song->repeat_time; i++)
  {
    printf ("Processus %d Thread %x : %s \n", pid, (unsigned int) tid, song->lyrics) ;
    usleep (rand() / RAND_MAX * 1000000.) ;
  }
  return (void*) tid ;
}

int main (int argc, char **argv)
{
  if (argc != 5)
  {
    fprintf(stderr, "usage : %s team1 team2 num1 num2\n", argv[0]) ;
    return -1;
  }

  int team1 = atoi(argv[1]);
  int team2 = atoi(argv[2]);
  int nb_threads = team1 + team2; 
  pthread_t* tids = malloc(nb_threads*sizeof(pthread_t)) ;
  int i = 0;
  
  team_song_t song1;
  song1.lyrics = "Allons enfants de la patrie";
  song1.repeat_time = atoi (argv[3]);
  
  team_song_t song2;
  song2.lyrics = "Swing low, sweet chariot";
  song2.repeat_time = atoi (argv[4]);

  /* Create the threads for team1 */
  for (i = 0 ; i < team1; i++)
  {
    pthread_create(&tids[i], NULL, supporter, &song1);
  }
  
  /* Create the threads for team2 */
  for ( ; i < nb_threads; i++)
  {
    pthread_create(&tids[i], NULL, supporter, &song2);
  }  

  /* Wait until every thread end */ 
  for (i = 0; i < nb_threads; i++)
  {
    pthread_join(tids[i], NULL);
  }
  
  free(tids);
  return EXIT_SUCCESS;
}
