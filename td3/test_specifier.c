#include <stdlib.h>
#include "my_stdio.h"
int main (int argc, char *argv[])
{
    MY_FILE *f1, *f2;
    f1 = my_fopen("test_specifier_input.txt", "r");
    if (f1 == NULL)
        exit (-2);
    f2 = my_fopen("/dev/stdout", "w");
	char string1[255];
	char string2[255];
		int integer;
	char character;
	int second_integer;
	char string3[255];
    my_fscanf(f1,"This is 2 strings: %s %s",string1,string2);
    my_fscanf(f1,"This is an integer: %d",&integer);
    my_fscanf(f1,"This is a character: %c",&character);
    my_fscanf(f1,"This is an integer just before a string: %d%s",&second_integer,string3);
    my_fprintf(f2,"The strings are %s and %s,\nthe integer is %d,\nthe character is %c,\nthe last sentence is %d%s\n",string1,string2,integer,character,second_integer,string3);
    
    my_fclose(f1);
    my_fclose(f2);
    return 0;
}
