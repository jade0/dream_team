#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>
#include <stdarg.h>

int main(int argc, char *argv[])
{
  int f1;
  int f2;
  char c;
  int result;

  // for the sake of simplicity we don't
  // print any error messages
  if (argc != 3)
      exit (-1);

  f1 = open(argv[1], O_RDONLY);
  if (f1 == -1)
      exit (-2);

  f2 = open(argv[2], O_WRONLY);
  if (f2 == -1)
      exit (-3);

  result = read(f1, &c, 1);
  while (result == 1)
    {
      result = write(f2, &c, 1);
      if (result == -1)
          exit(-4);
      result = read(f1, &c, 1);
    }
  if (result == -1)
      exit(-5);

  close(f1);
  close(f2);
  return 0;
}
