#include <stdlib.h>
#include <stdio.h>
int main (int argc, char *argv[])
{
    FILE *f1, *f2;
    f1 = fopen("test_specifier_input.txt", "r");
    if (f1 == NULL)
        exit (-2);
    f2 = fopen("/dev/stdout", "w");
	char string1[255];
	char string2[255];
	int integer;
	char character;
	int second_integer;
	char string3[255];
    fscanf(f1,"This is 2 strings: %s %s\n",string1,string2);
    fscanf(f1,"This is an integer: %d\n",&integer);
    fscanf(f1,"This is a character: %c\n",&character);
    fscanf(f1,"This is an integer just before a string: %d%s\n",&second_integer,string3);
    fprintf(f2,"The strings are %s and %s,\nthe integer is %d,\nthe character is %c,\nthe last sentence is %d%s\n",string1,string2,integer,character,second_integer,string3);
    fclose(f1);
    fclose(f2);
    return 0;
}
