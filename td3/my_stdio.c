#include "my_stdio.h"
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>
#include <stdarg.h>

#define BUFFER_SIZE 128
#define min(x, y) ((x < y) ? x : y)

struct MY_FILE
{
    int file_descriptor;
    int cursor_position;
    char buffer[BUFFER_SIZE];
    int end_of_file;
    MY_FILE* next;
};

MY_FILE* first_file = NULL;

MY_FILE* my_fopen(char* name, char* mode)
{
    MY_FILE* file = (MY_FILE*) malloc(sizeof(MY_FILE));
    if (strcmp(mode, "r") == 0) file->file_descriptor = open(name, O_RDONLY);
    else if (strcmp(mode, "w") == 0) file->file_descriptor = open(name, O_WRONLY);
    else
    {
        free(file);
        return NULL;
    }
    file->cursor_position = 0;
    file->end_of_file = INT_MAX;
    file->next = NULL;

    if (first_file == NULL)
    {
        first_file = file;
    }
    else
    {
        MY_FILE* tmp = first_file;
        while (tmp->next != NULL)
        {
            tmp = tmp->next;
        }
        tmp->next = file;
    }

    return file;
}

int my_fclose(MY_FILE* file)
{
    if (file == NULL)
    {
        return -1;
    }
    if (file == first_file)
    {
        first_file = first_file->next;
    }
    else
    {
        MY_FILE* tmp = first_file;
        while (tmp->next != file && tmp->next != NULL)
        {
            tmp = tmp->next;
        }
        if (tmp->next == NULL)
        {
            return -1;
        }
        else
        {
            tmp->next = tmp->next->next;
        }
    }
    write(file->file_descriptor, file->buffer, file->cursor_position); 

    close(file->file_descriptor);
    free(file);
    return 0;
}

int my_fread(void* ptr, size_t size, size_t nbelem, MY_FILE* file)
{
    size_t total_size = nbelem * size;
    ssize_t buffer_size_read;
    if(file->cursor_position == 0)
    {
        buffer_size_read = read(file->file_descriptor, (void*)file->buffer, (size_t)BUFFER_SIZE );
        if (buffer_size_read < BUFFER_SIZE) file->end_of_file = buffer_size_read -1;
    }

    if (my_feof(file) == 1) return 0;

    if(file->cursor_position + (int)total_size >= file->end_of_file)
	{
		int last_bytes = file->end_of_file - file->cursor_position;
		memcpy(ptr,file->buffer+file->cursor_position,last_bytes);
		file->cursor_position += last_bytes;	
		return last_bytes;

	}
    else if (file->cursor_position + total_size <= BUFFER_SIZE)
    {
        memcpy(ptr, file->buffer + file->cursor_position, total_size);
        file->cursor_position += total_size;
        file->cursor_position %= BUFFER_SIZE;
        return total_size;
    }
    else
    {
        int remaining_bytes = BUFFER_SIZE - file->cursor_position;
        memcpy(ptr, file->buffer + file->cursor_position, remaining_bytes);
        file->cursor_position += remaining_bytes;
        file->cursor_position %= BUFFER_SIZE;
        remaining_bytes += my_fread(ptr + remaining_bytes, 1, total_size - remaining_bytes, file);
        return remaining_bytes;
    }
}

int my_fwrite(void *p, size_t size, size_t nbelem, MY_FILE* file)
{
    size_t total_size = nbelem * size;

    if(file->cursor_position == 0)
    {
        memset(file->buffer, 0, BUFFER_SIZE);
    }
    if (file->cursor_position + total_size <= BUFFER_SIZE)
    {
        memcpy(file->buffer + file->cursor_position, p, total_size);
        file->cursor_position += total_size;
        file->cursor_position %= BUFFER_SIZE;
        if (file->cursor_position == 0)
        {
            write(file->file_descriptor, file->buffer, BUFFER_SIZE);
        }
        return total_size;
    }
    else
    {
         int remaining_bytes = BUFFER_SIZE - file->cursor_position;
         memcpy(file->buffer + file->cursor_position, p, remaining_bytes);
         file->cursor_position += remaining_bytes;
         file->cursor_position %= BUFFER_SIZE;
         if (file->cursor_position == 0)
         {
             write(file->file_descriptor, file->buffer, BUFFER_SIZE);
         }
         remaining_bytes += my_fwrite(p + remaining_bytes, 1, total_size - remaining_bytes, file);
         return remaining_bytes;
    }
}

int my_feof(MY_FILE* file)
{
    if (file == NULL) return -1;
    if (file->end_of_file == INT_MAX) return 0;
    if (file->cursor_position >= file->end_of_file) return 1;
    else return 0;
}

/**
 * itoa implementation found on http://www.jb.man.ac.uk/~slowe/cpp/itoa.html
 */
char* my_itoa(int val)
{
	static char buf[32] = {0};
	int i = 0;
	for(i = 30 ; val != 0 && i != 0 ; i--)
	{
		buf[i] = "0123456789"[val % 10];
		val /= 10;
	}
	return &buf[i+1];
}

int my_fprintf(MY_FILE* f, char* format, ...)
{
	va_list vl;
	va_start(vl, format);
	int n = 0;
	int i = 0;
    for (i = 0 ; i < (int)strlen(format) ; i++)
	{
		if (format[i] == '%')
		{
			if (format[i+1] == 'c')
			{
                char c = (char)va_arg(vl, int);
				my_fwrite(&c, sizeof(char), 1, f);
			}
			else if (format[i+1] == 's')
			{
				char* str = va_arg(vl, char*);
				my_fwrite(str, sizeof(char), strlen(str), f);
			}
			else if (format[i+1] == 'd')
			{
				int d = va_arg(vl, int);
				char* d_str = my_itoa(d);
				my_fwrite(d_str, sizeof(char), strlen(d_str), f);
			}
			else
			{
				return -1;
			}
			n++;
			i++;
		}
		else
		{
			char c = format[i];
			my_fwrite(&c, sizeof(char), 1, f);
		}
	}
	va_end(vl);
	return n;
}

int my_fscanf(MY_FILE* f, char* format, ...)
{
	va_list vl;
	va_start(vl, format);
	int n = 0;
	int i = 0;

    char c = ' ';

    for (i = 0 ; i < (int)strlen(format) ; i++)
	{
        while (format[i] == ' ' || format[i] == '\n' || format[i] == '\t')
        {
            i++;
        }
        while (c == ' ' || c == '\n' || format[i] == '\t')
        {
            my_fread(&c, sizeof(char), 1, f);
        }

        if (format[i] == '%')
        {
            if (format[i+1] == 'c')
            {
                char* c_arg = va_arg(vl, char*);
                *c_arg = c;
                c = ' ';
            }
            else if (format[i+1] == 's')
            {
                char* str = va_arg(vl, char*);
                int j = 0;
                while (c != ' ' && c !='\n' && c != '\t' && !my_feof(f))
                {
                    str[j] = c;
                    my_fread(&c, sizeof(char), 1, f);
                    j++;
                }
                if (my_feof(f))
                {
                	str[j]=c;
               		j++;
                }
                str[j] = '\0';

            }
            else if (format[i+1] == 'd')
            {
                int* d = va_arg(vl, int*);
                char d_str[10];
                int j = 0;
                while (c >= '0' && c <= '9' && !my_feof(f))
                {
                    d_str[j] = c;
                    my_fread(&c, sizeof(char), 1, f);
                    j++;
                }
                if (my_feof(f))
                {
                	d_str[j]=c;
               		j++;
                }
                d_str[j] = '\0';
                *d = atoi(d_str);
            }
            else
            {
                return -1;
            }
            n++;
            i++;
        }
        else
        {
            if (format[i] != c) return -1;
            c = ' ';
        }
	}
	va_end(vl);
	return n;
}






