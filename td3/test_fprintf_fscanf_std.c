#include <stdlib.h>
#include "my_stdio.h"
int main (int argc, char *argv[])
{
    FILE *f1, *f2;
    char c;
    // for the sake of simplicity we don’t
    // print any error messages
    if (argc != 3)
        exit (-1);
    f1 = fopen(argv[1], "r");
    if (f1 == NULL)
        exit (-2);
    f2 = fopen(argv[2], "w");
    if (f2 == NULL)
        exit (-3);
    fprintf(f2, "Input file: %s\n", argv[1]);
    fscanf(f1, "%c", &c);
    while (!feof(f1))
    {
        fprintf(f2, "Character %c read, its ASCII code is %d\n", c, c);
        fscanf(f1, "%c", &c);
    }
    fclose(f1);
    fclose(f2);
    return 0;
}
