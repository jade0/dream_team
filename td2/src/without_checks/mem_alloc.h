#ifndef   	_MEM_ALLOC_H_
#define   	_MEM_ALLOC_H_
#include <stdbool.h>

/* Allocator functions, to be implemented in mem_alloc.c */
void memory_init(void); 
char* memory_alloc(int size);
void memory_free(char* p);

/* Logging functions */
void print_info(void); 
void print_alloc_info(char* addr, int size);
void print_free_info(char* addr);
void print_free_blocks(void); 
char* heap_base(void);

/* Tells the allocator to use the "best fit" allocation strategy */
void use_best_fit_allocation(void);

/* Tells the allocator to use the "worst fit" allocation strategy */
void use_worst_fit_allocation(void);

/* Changes the alignement of newly created blocks */
void align_alloc(int alignment);

#include <stdlib.h>
//void *malloc(size_t size); 
//void free(void *p); 
//void *realloc(void *ptr, size_t size); 

#endif 	    /* !_MEM_ALLOC_H_ */
