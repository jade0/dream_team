#include "mem_alloc.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#ifndef MEMORY_SIZE
#define MEMORY_SIZE 512
#endif

#define ULONG(x) ((long unsigned int)(x))

#define max(x,y) (x>y?x:y)

/* Allocation strategy : "first", "best" or "worst" */
const char* allocation_strategy = "first";

/* Alignement of allocation spaces */
int alignment = 1;

/* Memory */
char memory[MEMORY_SIZE]; 

/* Structure declaration for a free block */
typedef struct free_block {
    int size;
    struct free_block *next;
} free_block_s, *free_block_t; 

/* Structure declaration for an occupied block */
typedef struct {
    int size;
} busy_block_s, *busy_block_t; 

/* Pointer to the first free block in the memory */
free_block_t first_free;

void memory_init(void)
{
    free_block_s first_header;
    first_header.size = MEMORY_SIZE;
    first_header.next = NULL;
    free_block_t destination = (free_block_t)&memory[0];
    *destination = first_header;
    first_free = destination;
}

/* Returns the first free block in memory of which its size is enough
   to contain the user data */
free_block_t get_first_available(int size)
{
    free_block_t first_available = first_free;
    while (first_available != NULL && first_available->size < size)
    {
        first_available = first_available->next;
    }
    return first_available;
}

/* Return the best free block in memory (according to the "best" strategy) */
free_block_t get_best_available(int size)
{
    free_block_t best_available = NULL;
    free_block_t tmp = first_free;
    while (tmp != NULL)
    {
        if (best_available == NULL ||
                (tmp->size >= size &&
                 tmp->size - size < best_available->size - size))
        {
            best_available = tmp;
        }
        tmp = tmp->next;
    }
    return best_available;
}

/* Return the worst free block in memory (according to the "worst" strategy) */
free_block_t get_worst_available(int size)
{
    free_block_t worst_available = NULL;
    free_block_t tmp = first_free;
    while (tmp != NULL)
    {
        if (worst_available == NULL ||
                (tmp->size >= size &&
                 tmp->size - size > worst_available->size - size))
        {
            worst_available = tmp;
        }
        tmp = tmp->next;
    }
    return worst_available;
}

/* Returns the previous free block of a specific free block in memory */
free_block_t get_previous_free_block(free_block_t block)
{
    free_block_t tmp = first_free;
    while (tmp != NULL && tmp->next != block)
    {
        tmp = tmp->next;
    }
    return tmp;
}

char* memory_alloc(int size)
{
    // size must be enough to store a free_block_s
    size += sizeof(busy_block_s);
    size = max((int)sizeof(free_block_s), size);
    // size = max(16, size);

    // align allocation block
    if (size % alignment != 0) size = ((size / alignment) + 1) * alignment;

    // choose the free block according to the strategy (first, best or worst)
    free_block_t block_available = NULL;
    if (strcmp(allocation_strategy, "first") == 0)
        block_available = get_first_available(size);
    else if (strcmp(allocation_strategy, "best") == 0)
        block_available = get_best_available(size);
    else if (strcmp(allocation_strategy, "worst") == 0)
        block_available = get_worst_available(size);

    if (block_available == NULL) return NULL;

    free_block_t prec = get_previous_free_block(block_available);
    busy_block_s busy;
    busy.size = size;
    int remaining_size = block_available->size - size;

    // if we cannot create a new free block in the remaining free space,
    // then we extend the busy block to take all the remaining space
    if (remaining_size < (int)sizeof(free_block_s))
    {
        busy.size += remaining_size;
        if (prec == NULL) first_free = first_free->next;
        else prec->next = block_available->next;
    }
    // else we create a new free block after the busy block
    else
    {
        free_block_s new_free_block;
        new_free_block.next = block_available->next;
        new_free_block.size = remaining_size;

        free_block_t destination = (free_block_t) ((char*)block_available + size);
        *destination = new_free_block;
        if (prec == NULL) first_free = destination;
        else prec->next = destination;
    }

    // put the new busy block in place
    busy_block_t dest = (busy_block_t)block_available;
    *dest = busy;

    // give back the address to the user
    char* addr = (char*) block_available + sizeof(busy_block_s);
    print_alloc_info(addr, size - sizeof(busy_block_s));

    return addr;
}

/* Put a new free block instead of a busy block at address addr */
void make_free_block(char* addr)
{
    free_block_s header;
    header.size = ((busy_block_t)addr)->size;
    header.next = NULL;
    free_block_t destination = (free_block_t)addr;
    *destination = header;
}

/* Link a newly created free block with the rest of the free blocks */
void link_free_block(char* addr)
{
    free_block_t block = (free_block_t)addr;
    if ((char*)first_free > addr)
    {
        block->next = first_free;
        first_free = block;
    }
    else
    {
        free_block_t prec = first_free;
        while (prec->next != NULL && (char*)prec->next < addr)
        {
            prec = prec->next;
        }
        block->next = prec->next;
        prec->next = block;
    }
}

/* Merge a free block between two free blocks */
void merge_free_block(char* addr)
{
    free_block_t block = (free_block_t)addr;
    free_block_t prec = first_free;
    free_block_t next = prec->next;
    while (prec->next != NULL && (char*)prec->next < addr)
    {
        prec = prec->next;
        next = next->next;
    }
    if (next == block)
    {
        next = block->next;
    }
    if (next != NULL && (char*)block + block->size == (char*)next)
    {
        block->size = block->size + next->size;
        block->next = next->next;
    }
    if ((char*)prec + prec->size == addr)
    {
        prec->size = prec->size + block->size;
        prec->next = block->next;
    }
}

void memory_free(char* p)
{
    char* real_free_addr = p - sizeof(busy_block_s);
    make_free_block(real_free_addr);
    link_free_block(real_free_addr);
    merge_free_block(real_free_addr);
    print_free_info(p);
}

void print_info(void)
{
    fprintf(stderr, "Memory : [%lu %lu] (%lu bytes)\n", (long unsigned int) 0, (long unsigned int) (memory+MEMORY_SIZE), (long unsigned int) (MEMORY_SIZE));
    fprintf(stderr, "Free block : %lu bytes; busy block : %lu bytes.\n", ULONG(sizeof(free_block_s)), ULONG(sizeof(busy_block_s)));
}

void print_free_info(char *addr)
{
    if(addr)
        fprintf(stderr, "FREE  at : %lu \n", ULONG(addr - memory));
    else
        fprintf(stderr, "FREE  at : %lu \n", ULONG(0));
}

void print_alloc_info(char *addr, int size)
{
    if(addr)
    {
        fprintf(stderr, "ALLOC at : %lu (%d byte(s))\n",
                ULONG(addr - memory), size);
    }
    else
    {
        fprintf(stderr, "Warning, system is out of memory\n");
    }
}

void print_free_blocks(void)
{
    free_block_t current;
    fprintf(stderr, "Begin of free block list :\n");
    for(current = first_free; current != NULL; current = current->next)
        fprintf(stderr, "Free block at address %lu, size %u\n", ULONG((char*)current - memory), current->size);
}

char *heap_base(void)
{
    return memory;
}

void use_best_fit_allocation()
{
    allocation_strategy = "best";
}

void use_worst_fit_allocation()
{
    allocation_strategy = "worst";
}

void align_alloc(int align)
{
    alignment = align;
}

void *malloc(size_t size){
    static int init_flag = 0;
    if(!init_flag){
        init_flag = 1;
        memory_init();
        //print_info();
    }
    return (void*)memory_alloc((size_t)size);
}

void free(void *p){
    if (p == NULL) return;
    memory_free((char*)p);
    print_free_blocks();
}

void *realloc(void *ptr, size_t size){
    if(ptr == NULL)
        return memory_alloc(size);
    busy_block_t bb = ((busy_block_t)ptr) - 1;
    printf("Reallocating %d bytes to %d\n", bb->size - (int)sizeof(busy_block_s), (int)size);
    if(size <= bb->size - sizeof(busy_block_s))
        return ptr;

    char *new = memory_alloc(size);
    memcpy(new, (void*)(bb+1), bb->size - sizeof(busy_block_s) );
    memory_free((char*)(bb+1));
    return (void*)(new);
}

#ifdef MAIN
int main(void)
{
    memory_init();

    memory_alloc(20);
    print_free_blocks();
    char* a28 = memory_alloc(20);
    print_free_blocks();
    char* a52 = memory_alloc(12);
    print_free_blocks();
    char* a68 = memory_alloc(15);
    print_free_blocks();
    memory_alloc(20);
    print_free_blocks();
    memory_free(a28);
    print_free_blocks();
    memory_free(a68);
    print_free_blocks();
    memory_free(a52);
    print_free_blocks();
    return EXIT_SUCCESS;
}
#endif
