#include "mem_alloc.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#ifndef MEMORY_SIZE
#define MEMORY_SIZE 512
#endif

#define ULONG(x) ((long unsigned int)(x))

#define max(x,y) (x>y?x:y)

/* Allocation strategy : "first", "best" or "worst" */
const char* allocation_strategy = "first";

/* Alignement of allocation spaces */
int alignment = 1;

/* Memory */
char memory[MEMORY_SIZE]; 

/* Structure declaration for a free block */
typedef struct free_block {
    int size;
    struct free_block *next;
} free_block_s, *free_block_t; 

/* Structure declaration for an occupied block */
typedef struct {
    int size;
} busy_block_s, *busy_block_t; 

/* Pointer to the first free block in the memory */
free_block_t first_free;

void memory_init(void)
{
    free_block_s first_header;
    first_header.size = MEMORY_SIZE;
    first_header.next = NULL;
    free_block_t destination = (free_block_t)&memory[0];
    *destination = first_header;
    first_free = destination;
}

int check_memory_consistency(void)
{
    char* memory_begin = (char*) &memory[0];
    char* memory_end = (char*) &memory[MEMORY_SIZE - 1];
    free_block_t free_block = first_free;
    while (free_block != NULL)
    {
        char* free_block_addr = (char*) free_block;
        if (free_block_addr < memory_begin || free_block_addr > memory_end)
        {
            printf("Warning : free block at address %p "
                   "has an inconsistent address !", free_block_addr);
            return 1;
        }
        if (free_block->size < 0 || free_block->size > MEMORY_SIZE)
        {
            printf("Warning : the size of the free block at "
                   "address %p is inconsistent !", free_block_addr);
            return 2;
        }
        free_block = free_block->next;
    }
    return 0;
}

/* Returns the first free block in memory of which its size is enough
   to contain the user data */
free_block_t get_first_available(int size)
{
    free_block_t first_available = first_free;
    while (first_available != NULL && first_available->size < size)
    {
        first_available = first_available->next;
    }
    return first_available;
}

/* Return the best free block in memory (according to the "best" strategy) */
free_block_t get_best_available(int size)
{
    free_block_t best_available = NULL;
    free_block_t tmp = first_free;
    while (tmp != NULL)
    {
        if (best_available == NULL ||
                (tmp->size >= size &&
                 tmp->size - size < best_available->size - size))
        {
            best_available = tmp;
        }
        tmp = tmp->next;
    }
    return best_available;
}

/* Return the worst free block in memory (according to the "worst" strategy) */
free_block_t get_worst_available(int size)
{
    free_block_t worst_available = NULL;
    free_block_t tmp = first_free;
    while (tmp != NULL)
    {
        if (worst_available == NULL ||
                (tmp->size >= size &&
                 tmp->size - size > worst_available->size - size))
        {
            worst_available = tmp;
        }
        tmp = tmp->next;
    }
    return worst_available;
}

/* Returns the previous free block of a specific free block in memory */
free_block_t get_previous_free_block(free_block_t block)
{
    free_block_t tmp = first_free;
    while (tmp != NULL && tmp->next != block)
    {
        tmp = tmp->next;
    }
    return tmp;
}

char* memory_alloc(int size)
{
    // first check the consistency of the memory
    if (check_memory_consistency() != 0) exit(0);

    // size must be enough to store a free_block_s
    size += sizeof(busy_block_s);
    size = max((int)sizeof(free_block_s), size);
    size = max(16, size);
    
    // align allocation block
    if (size % alignment != 0) size = ((size / alignment) + 1) * alignment;

    // choose the free block according to the strategy (first, best or worst)
    free_block_t block_available = NULL;
    if (strcmp(allocation_strategy, "first") == 0)
        block_available = get_first_available(size);
    else if (strcmp(allocation_strategy, "best") == 0)
        block_available = get_best_available(size);
    else if (strcmp(allocation_strategy, "worst") == 0)
        block_available = get_worst_available(size);

    if (block_available == NULL) return NULL;

    free_block_t prec = get_previous_free_block(block_available);
    busy_block_s busy;
    busy.size = size;
    int remaining_size = block_available->size - size;

    // if we cannot create a new free block in the remaining free space,
    // then we extend the busy block to take all the remaining space
    if (remaining_size < (int)sizeof(free_block_s))
    {
        busy.size += remaining_size;
        if (prec == NULL) first_free = first_free->next;
        else prec->next = block_available->next;
    }
    // else we create a new free block after the busy block
    else
    {
        free_block_s new_free_block;
        new_free_block.next = block_available->next;
        new_free_block.size = remaining_size;

        free_block_t destination = (free_block_t) ((char*)block_available + size);
        *destination = new_free_block;
        if (prec == NULL) first_free = destination;
        else prec->next = destination;
    }

    // put the new busy block in place
    busy_block_t dest = (busy_block_t)block_available;
    *dest = busy;

    // give back the address to the user
    char* addr = (char*) block_available + sizeof(busy_block_s);
    print_alloc_info(addr, size - sizeof(busy_block_s));

    return addr;
}

/* Put a new free block instead of a busy block at address addr */
void make_free_block(char* addr)
{
    free_block_s header;
    header.size = ((busy_block_t)addr)->size;
    header.next = NULL;
    free_block_t destination = (free_block_t)addr;
    *destination = header;
}

/* Link a newly created free block with the rest of the free blocks */
void link_free_block(char* addr)
{
    free_block_t block = (free_block_t)addr;
    if ((char*)first_free > addr)
    {
        block->next = first_free;
        first_free = block;
    }
    else
    {
        free_block_t prec = first_free;
        while (prec->next != NULL && (char*)prec->next < addr)
        {
            prec = prec->next;
        }
        block->next = prec->next;
        prec->next = block;
    }
}

/* Merge a free block between two free blocks */
void merge_free_block(char* addr)
{
    free_block_t block = (free_block_t)addr;
    free_block_t prec = first_free;
    free_block_t next = prec->next;
    while (prec->next != NULL && (char*)prec->next < addr)
    {
        prec = prec->next;
        next = next->next;
    }
    if (next == block)
    {
        next = block->next;
    }
    if (next != NULL && (char*)block + block->size == (char*)next)
    {
        block->size = block->size + next->size;
        block->next = next->next;
    }
    if ((char*)prec + prec->size == addr)
    {
        prec->size = prec->size + block->size;
        prec->next = block->next;
    }
}

/* Checks whether the given address is in a free block */
bool is_addr_in_free_block(char* p)
{
    free_block_t tmp = first_free;
    while (tmp != NULL && (p < (char*)tmp || p > (char*)tmp + tmp->size))
    {
        tmp=tmp->next;
    }
    if (tmp == NULL)
    {
        return false;
    }
    else return true;
}

/* Checks whether the given address is in a middle of a busy block*/
bool is_addr_in_busy_block(char* p)
{
    free_block_t tmp = first_free;
    if ((char*)tmp > p)
    {
        busy_block_t first_busy = (busy_block_t)&memory[0];
        if ((char*)first_busy + sizeof(busy_block_s) == p) return false;
        while((char*)first_busy + first_busy->size != (char*)first_free)
        {
            if ((char*)first_busy + sizeof(busy_block_s) > p) return true;
            if ((char*)first_busy + sizeof(busy_block_s) == p) return false;
            if ((char*)first_busy + sizeof(busy_block_s) < p &&
                    (char*)first_busy + first_busy->size > p) return true;
            first_busy=(busy_block_t)((char*)first_busy+first_busy->size);
        }
        return true;
    }
    else
    {
        while(tmp !=NULL)
        {
            free_block_t tmp_next = (tmp->next == NULL) ? (free_block_t)&memory[MEMORY_SIZE - 1] : tmp->next;
            if(p > (char*)tmp && p< (char*) tmp_next)
            {
                if ((char*) tmp + tmp->size + sizeof(busy_block_s)!= p)
                {
                    busy_block_t first_busy = (busy_block_t)((char*) tmp + tmp->size);
                    while((char*)first_busy + first_busy->size != (char*)tmp->next)
                    {
                        if ((char*)first_busy + sizeof(busy_block_s) > p) return true;
                        if ((char*)first_busy + sizeof(busy_block_s) == p) return false;
                        if ((char*)first_busy + sizeof(busy_block_s) < p &&
                                (char*)first_busy + first_busy->size > p) return true;
                        first_busy=(busy_block_t)((char*)first_busy+first_busy->size);
                    }
                }
                else return false;
            }
            tmp=tmp->next;
        }
        return false;
    }
}

void memory_free(char* p)
{
    // first check the consistency of the memory
    if (check_memory_consistency() != 0) exit(0);

    char* real_free_addr = p - sizeof(busy_block_s);
    bool in_free_block = is_addr_in_free_block(p);
    bool in_busy_block = is_addr_in_busy_block(p);
    if (!in_free_block && !in_busy_block)
    {
        make_free_block(real_free_addr);
        link_free_block(real_free_addr);
        merge_free_block(real_free_addr);
    }
    else if (in_free_block)
    {
        fprintf(stderr, "WARNING : Liberation of a block \n of memory inside a free block \n");
    }
    else if (in_busy_block)
    {
        fprintf(stderr, "WARNING : Liberation of a block \n of memory in the middle of a busy block\n");
    }
    print_free_info(p);
}

void print_info(void)
{
    fprintf(stderr, "Memory : [%lu %lu] (%lu bytes)\n", (long unsigned int) 0, (long unsigned int) (memory+MEMORY_SIZE), (long unsigned int) (MEMORY_SIZE));
    fprintf(stderr, "Free block : %lu bytes; busy block : %lu bytes.\n", ULONG(sizeof(free_block_s)), ULONG(sizeof(busy_block_s)));
}

void print_free_info(char *addr)
{
    if(addr)
        fprintf(stderr, "FREE  at : %lu \n", ULONG(addr - memory));
    else
        fprintf(stderr, "FREE  at : %lu \n", ULONG(0));
}

void print_alloc_info(char *addr, int size)
{
    if(addr)
    {
        fprintf(stderr, "ALLOC at : %lu (%d byte(s))\n",
                ULONG(addr - memory), size);
    }
    else
    {
        fprintf(stderr, "Warning, system is out of memory\n");
    }
}

void print_free_blocks(void)
{
    free_block_t current;
    fprintf(stderr, "Begin of free block list :\n");
    for(current = first_free; current != NULL; current = current->next)
        fprintf(stderr, "Free block at address %lu, size %u\n", ULONG((char*)current - memory), current->size);
}

char *heap_base(void)
{
    return memory;
}

void use_best_fit_allocation()
{
    allocation_strategy = "best";
}

void use_worst_fit_allocation()
{
    allocation_strategy = "worst";
}

void align_alloc(int align)
{
    alignment = align;
}

int memory_check()
{
    if (first_free->size == MEMORY_SIZE) return 0;
    else
    {
        fprintf(stderr, "%s\n","WARNING : All allocations haven't been freed");
        return 1;
    }
}

void *malloc(size_t size){
    static int init_flag = 0;
    if(!init_flag){
        init_flag = 1;
        memory_init();
        //print_info();
    }
    return (void*)memory_alloc((size_t)size);
}

void free(void *p){
    if (p == NULL) return;
    memory_free((char*)p);
    print_free_blocks();
}

void *realloc(void *ptr, size_t size){
    if(ptr == NULL)
        return memory_alloc(size);
    busy_block_t bb = ((busy_block_t)ptr) - 1;
    printf("Reallocating %d bytes to %d\n", bb->size - (int)sizeof(busy_block_s), (int)size);
    if(size <= bb->size - sizeof(busy_block_s))
        return ptr;

    char *new = memory_alloc(size);
    memcpy(new, (void*)(bb+1), bb->size - sizeof(busy_block_s) );
    memory_free((char*)(bb+1));
    return (void*)(new);
}

#ifdef MAIN
int main(void)
{
    memory_init();

    memory_alloc(20);
    print_free_blocks();
    char* a28 = memory_alloc(20);
    print_free_blocks();
    char* a52 = memory_alloc(12);
    print_free_blocks();
    char* a68 = memory_alloc(15);
    print_free_blocks();
    memory_alloc(20);
    print_free_blocks();
    memory_free(a28);
    print_free_blocks();
    memory_free(a68);
    print_free_blocks();
    memory_free(a52);
    print_free_blocks();
    int success = memory_check();
    if (success == 0)    return EXIT_SUCCESS;
    else return EXIT_FAILURE;
}
#endif
